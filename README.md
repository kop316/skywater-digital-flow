# Overview
This repository runs the following pipecleaner designs through a digital physical design flow using Design Compiler and Innovus with the SkyWater open source 130nm PDK.
*  GcdUnit - computes the greatest common divisor function, consists of 100-200 gates
*  SramUnit - uses an OpenRAM generated SRAM, plus a simple counter that supplies addresses to it

# Setup
To run this flow, please install the following dependencies first in this order:

1. `skywater-pdk` 

Get SkyWater PDK:
```
git clone https://github.com/google/skywater-pdk.git
cd skywater-pdk
```
The cell libraries are in submodules that need to be checked out independently:
```
git submodule update --init libraries/sky130_fd_sc_hd/latest
git submodule update --init libraries/sky130_fd_pr/latest
git submodule update --init libraries/sky130_fd_io/latest
```
To create the .lib timing files:
```
make timing
cd ..
```

2. `magic`, `open_pdks`, `netgen`, `iverilog`

We are using git tags to have some source control. Since we are building from source, everything
is going to be stored in an `/opt/&APP` folder, so it is easier to remove.

We are following the instruction guide here http://opencircuitdesign.com/magic/ 
```
git clone https://github.com/RTimothyEdwards/magic.git
cd magic
git fetch --all --tags
git checkout tags/8.3.270
./configure --prefix=/opt/magic
make
make install
```
We are following the instruction guide here: http://opencircuitdesign.com/open_pdks/

```
git clone https://github.com/RTimothyEdwards/open_pdks.git
cd open_pdks
git fetch --all --tags
git checkout tags/1.0.289
./configure --enable-sky130-pdk=`realpath ../skywater-pdk/libraries` --prefix=/opt/open_pdks
make
make install
```

We are following the instruction guide here: http://opencircuitdesign.com/netgen

```
git https://github.com/RTimothyEdwards/netgen.git
cd netgen
git fetch --all --tags
git checkout tags/1.5.219
./configure --prefix=/opt/magic
make
make install
```

The instruction guide is here: https://github.com/steveicarus/iverilog

```
git clone https://github.com/steveicarus/iverilog.git
cd verilof
git fetch --all --tags
git checkout tags/v11_0
sh autogen.sh
./configure --prefix=/opt/iverilog
make
make install
```


3. `mflowgen` - This is a tool to create ASIC design flows in a modular fashion.
Follow the setup steps at https://mflowgen.readthedocs.io/en/latest/quick-start.html.

4. `skywater-130nm-adk` - This repo has some scripts that convert the SkyWater PDK into the format that mflowgen expects. Follow the setup steps at https://code.stanford.edu/ee272/skywater-130nm-adk. The files that are in `skywater-130nm-adk/view-standard` are the ones that mflowgen will use. (This is configured in the `design/construct.py` file for each pipecleaner.)

5. `mflowgen` expects the skywater-130nm-pdk to be in `mflowgen/adks/` the easiest way to do that is to do a synbolic link:

`ln -s ${ABSOLUTE_PATH_TO_skywater-130nm-adk} ${MFLOWGEN_ROOT_FOLDER}/adks/skywater-130nm-adk.v2021`

# Using the Pipecleaners

First, make sure you update various install paths in the `setup.bashrc` file. Then source it.
```
bash
source setup.bashrc
```

Next, enter into the build directory of the pipecleaner you want to run, and run the following:
```
cd GcdUnit/build
mflowgen run --design ../design/
```

Now, if you do `make status` you will see the status of all the steps in the flow. Use the following make targets to run and debug each step. For example to run step number N do:
```
make N
```

# Helpful make Targets
*  `make list` - list all the nodes in the graphs and their corresponding step number
*  `make status` - list the build status of all the steps
*  `make graph` - generates a PDF of the graph
*  `make N` - runs step N
*  `make debug-N` - pulls up GUI for the appropriate tool for debugging (at the output of step N)
*  `make clean-N` - removes the folder for step N, and sets the status of steps [N,) to build
*  `make clean-all` - removes folders for all the steps
*  `make runtimes` - lists the runtime for each step
