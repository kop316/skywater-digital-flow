ARGS=`cat inputs/design.args`

# Grab all design/testbench files
for f in inputs/*.v; do
    [ -e "$f" ] || continue
    ARGS="$ARGS $f"
done

for f in inputs/*.sv; do
    [ -e "$f" ] || continue
    ARGS="$ARGS $f"
done

# Set-up testbench
ARGS="$ARGS -s $testbench_name"

(
    set -x;
    iverilog -o testbench.vvp $ARGS && \
    vvp testbench.vvp
)

mv run.vcd outputs/run.vcd
# Iverilog adds timestamps to the output...which is kind of annoying
sed '/^\/\//d' output.txt > output_san.txt
mv output_san.txt outputs/output.txt
echo "Seeing difference of input and output..."
diff outputs/output.txt inputs/output_gold.txt
echo "Finished seeing difference of input and output..."
