module control
    #(parameter WL = 8)
    (input  logic   		clk,
    input   logic   		rst_b,
    input   logic   		ops_val,
    output  logic   		ops_rdy,
    input   logic   		res_rdy,
    output  logic   		res_val,
    input  	logic   [1:0]	compare_result,
    output  logic 			op_a_wen,
	output  logic 			op_b_wen,
	output  logic 			op_a_mux,
	output  logic 			op_b_mux);

	enum {
		GET_OPERAND,
		COMPUTE,
		WRITE_BACK
	} state, next_state;

    // Reset logic
	always_ff @ (posedge clk, negedge rst_b) begin
		if(!rst_b) begin
				state <= GET_OPERAND;
			end
		else
			state <= next_state;
	end

	// Cycle Logic
	always_comb begin
		next_state = state;
		case (state)
			GET_OPERAND: begin
				if (ops_val == 1) begin
					next_state = COMPUTE;
				end
			end
			COMPUTE: begin
				if (compare_result == 2'b11)
					next_state = WRITE_BACK;
			end
			WRITE_BACK: begin
				if (res_rdy == 1)
					next_state = GET_OPERAND;
			end
			default:
				next_state = state;
		endcase
	end

	// Stuff I actually do per cycle
	always_comb begin
		res_val = 0;
		ops_rdy = 0;
		op_a_wen = 0;
		op_b_wen = 0;
		op_a_mux = 0;
		op_b_mux = 0;
		case (state)
			GET_OPERAND: begin
				ops_rdy = 1;
				if (ops_val == 1) begin 	//There are operands waiting on us!
					op_a_wen = 1;
					op_b_wen = 1;
				end
			end
			COMPUTE: begin
				case (compare_result)
				2'b01: begin
					op_a_wen = 1;
					op_a_mux = 1;
				end
			    2'b10: begin
					op_b_wen = 1;
					op_b_mux = 1;
				end
				endcase
			end
			WRITE_BACK: begin
				res_val = 1;
				end
		endcase
	end
endmodule: control
