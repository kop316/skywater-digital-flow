module chip
    (input  wire            clk,
    input   wire            rst_b,
    input   wire    [7:0]   op_a,
    input   wire    [7:0]   op_b,
    output  wire    [7:0]   res,
    input   wire            ops_val,
    output  wire            ops_rdy,
    input   wire            res_rdy,
    output  wire            res_val);

    // Add datapath <-> control signals here
    wire   [1:0]		compare_result_signal;
    wire 				op_a_wen_signal;
	wire 				op_b_wen_signal;
	wire 				op_a_mux_signal;
	wire 				op_b_mux_signal;

    control #(.WL(8)) inst_control (.clk(clk),
    								.rst_b(rst_b),
    								.ops_val(ops_val),
    								.ops_rdy(ops_rdy),
    								.res_rdy(res_rdy),
    								.res_val(res_val),
    								.compare_result(compare_result_signal),
    								.op_a_wen(op_a_wen_signal),
									.op_b_wen(op_b_wen_signal),
									.op_a_mux(op_a_mux_signal),
									.op_b_mux(op_b_mux_signal));

    datapath #(.WL(8)) inst_datapath (.clk(clk),
    								  .rst_b(rst_b),
    								  .op_a(op_a),
    								  .op_b(op_b),
    								  .res(res),
    								  .compare_result(compare_result_signal),
    								  .op_a_wen(op_a_wen_signal),
									  .op_b_wen(op_b_wen_signal),
									  .op_a_mux(op_a_mux_signal),
									  .op_b_mux(op_b_mux_signal));

endmodule: chip
