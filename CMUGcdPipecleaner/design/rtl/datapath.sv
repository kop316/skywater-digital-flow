module datapath
    #(parameter WL = 8)
    (input  logic               clk,
     input  logic               rst_b,
     input  logic   [WL-1:0]    op_a,
     input  logic   [WL-1:0]    op_b,
     output logic   [WL-1:0]    res,
     output	logic   [1:0]		compare_result,
     input logic 				op_a_wen,
	 input logic 				op_b_wen,
	 input logic 				op_a_mux,
	 input logic 				op_b_mux);



	logic [WL-1:0] op_a_d;
	logic [WL-1:0] op_a_q;

	logic [WL-1:0] op_b_d;
	logic [WL-1:0] op_b_q;
	logic [WL-1:0] a_subtract_result;
	logic [WL-1:0] b_subtract_result;

	assign res = op_a_q;

	register_datapath #(.WL(8)) op_a_reg (.clk(clk),
							 	.rst_b(rst_b),
							 	.d(op_a_d),
							 	.q(op_a_q),
							 	.wen(op_a_wen));

	register_datapath #(.WL(8)) op_b_reg (.clk(clk),
										  .rst_b(rst_b),
										  .d(op_b_d),
										  .q(op_b_q),
										  .wen(op_b_wen));


	always_comb begin
		a_subtract_result = op_a_q - op_b_q;
		b_subtract_result = op_b_q - op_a_q;

		if (op_a_q > op_b_q)
			compare_result = 2'b01;
		else if (op_a_q < op_b_q)
			compare_result = 2'b10;
		else if (op_a_q == op_b_q)
			compare_result = 2'b11;
		else
			compare_result = 2'b00;

		if (op_a_mux)
			op_a_d = a_subtract_result;
		else
			op_a_d = op_a;

		if (op_b_mux)
			op_b_d = b_subtract_result;
		else
			op_b_d = op_b;
	end

endmodule: datapath
